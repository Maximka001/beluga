<?php
$main    = file_get_contents('main.html');
$welcome = file_get_contents('welcome.html');
$chousen = file_get_contents('choice.html');
$black_page = file_get_contents('noir.html');

// Age
if (isset($_REQUEST['age']) && !empty($_REQUEST['age'])) {
    setcookie('is_adult', 1, time() + 3600 * 24);
    $_COOKIE['is_adult'] = 1;
}

// Get language
$defaultLang = 'EN';
// $content = $chousen;

if (isset($_GET['lang']) && !empty($_GET['lang'])) {
    setcookie('lang', $_GET['lang'], time() + 3600 * 24);
    $lang = $_GET['lang'];
} elseif (isset($_POST['lang']) && !empty($_POST['lang'])) {
    setcookie('lang', $_POST['lang'], time() + 3600 * 24);
    $lang = $_POST['lang'];
} else {
    $lang = (isset($_COOKIE['lang']) && !empty($_COOKIE["lang"])) ? $_COOKIE['lang'] : $defaultLang;
}

if (isset($_REQUEST['chousen']) && !empty($_REQUEST['chousen'])) {
    setcookie('is_chos', $_POST['chousen'], 0);
    $_COOKIE['is_chos'] = $_POST['chousen'];
}

$translate = include strtolower($lang).'.php';

if ($_COOKIE['is_adult'] != '1') {
    // $whereInsert = strripos($main, '<div class="b-wrapper">');
    // $content = substr_replace($main, $welcome, $whereInsert + 23, 0);
    $content = $welcome;
} else {
    
    if ($_COOKIE['is_chos'] == 'WT') {
        $content = $main; // Тут будет выбор белая белуга или черная, над сделать как отправка по форме

    } else if ($_COOKIE['is_chos'] == 'BK') {
        $content = $black_page;

    } else {
        $content = $chousen;
    }
}

echo preg_replace(array_keys($translate), array_values($translate), $content);
?>