require.config({
    baseUrl: 'js',
    paths: {
        jquery: 'jquery/jquery',
        html5shiv: 'bower_components/html5shiv/dist/html5shiv',
        'jquery-placeholder': 'bower_components/jquery-placeholder/jquery.placeholder',
        'jquery-ui': 'bower_components/jquery-ui/jquery-ui',
        modernizr: 'bower_components/modernizr/modernizr',
        bootstrap: 'bower_components/bootstrap/dist/js/bootstrap',
        'jquery-hashchange': 'bower_components/jquery-hashchange/jquery.ba-hashchange',
        'jquery-mobile': 'bower_components/jquery-mobile/js/jquery.mobile',
        bower: 'bower_components/bower/atom-full-compiled',
        'es6-promise': 'bower_components/es6-promise/promise',
        install: 'bower_components/install/detect-zoom',
        rxjs: 'bower_components/rxjs/dist/rx.all',
        'jquery-shadow-animation': 'bower_components/jquery-shadow-animation/jquery.animate-shadow',
        'jquery-touchswipe': 'bower_components/jquery-touchswipe/jquery.touchSwipe',
        'jquery-validation': 'bower_components/jquery-validation/dist/jquery.validate',
        'jquery.maskedinput': 'bower_components/jquery.maskedinput/dist/jquery.maskedinput',
        select2: 'bower_components/select2/dist/js/select2',
        fullpage: 'bower_components/fullpage.js/dist/jquery.fullpage',
        'jquery.fullpage': 'bower_components/fullpage.js/dist/jquery.fullpage',
        'jquery.easings': 'bower_components/fullpage.js/vendors/jquery.easings.min',
        scrolloverflow: 'bower_components/fullpage.js/vendors/scrolloverflow.min',
        bootstrapTab: 'bower_components/bootstrap/js/tab',
        'slick-slider': 'external/slick/slick.min'
    },
    packages: [

    ],
    shim: {
        bootstrapCarousel: [
            'jquery'
        ],
        bootstrapModal: [
            'jquery'
        ],
        bootstrapCollapse: [
            'jquery'
        ],
        bootstrapTransition: [
            'jquery'
        ],
        bootstrapTooltip: [
            'jquery'
        ],
        bootstrapPopover: [
            'jquery',
            'bootstrapTooltip'
        ],
        bootstrapTab: [
            'jquery'
        ],
        'jquery-ui': [
            'jquery'
        ],
        'jquery.maskedinput': [
            'jquery'
        ],
        'bootstrap-carousel-swipe': [
            'jquery'
        ],
        'slick-carousel': [
            'jquery'
        ],
        'jquery-shadow-animation': [
            'jquery'
        ],
        fullpage: [
            'jquery'
        ],
        'slick-slider': [
            'jquery'
        ]
    }
});

//- Модули, подключаемые на всех страницах
require([
    'modernizr'
]);