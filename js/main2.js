require(['./config',], function() {
    require([
        'partials/select',
        'partials/fullpage2',
        // 'partials/share',
        'partials/preloader',
        'partials/height',
        'partials/chousen-mouse',
        'partials/chousen-form-sub',
        'partials/slider'
    ]);
});