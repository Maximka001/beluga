define(["jquery", '../partials/is-mobile'], function ($, isMobile) {
    var $link = $('[data-target-form]');
    var getSideMouseY = function (y) {

        if ((window.innerHeight / 2) < y) {
            return 'form_black';
        } else {
            return 'form_white';
        }
    };
    var getSideMouseX = function (x) {

        if ((window.innerWidth / 2) < x) {
            return 'form_black';
        } else {
            return 'form_white';
        }
    };
    var clickForm = function (event) {
        var $form = $('#' + $(this).data('target-form'));
        event.preventDefault();

        if ($form.length) {
            $form.submit();
        }
    };

    if ($link.length) {
        $link.on('click', function (event) {

            if (window.innerWidth >= 427 || $link.length == 1) {
                clickForm.call(this, event);
            }
        });

        $(document).on('click', function (event) {

            if (isMobile()) {

                if (window.innerWidth < 427) {
                    var $a = $('[data-target-form="' + getSideMouseY(event.clientY) + '"]');
                    if ($a.length) {
                        clickForm.call($a[0], event);
                    }
    
                } else {
                    var $a = $('[data-target-form="' + getSideMouseX(event.clientX) + '"]');
                    if ($a.length) {
                        clickForm.call($a[0], event);
                    }
                }
            }
        });
    }
});