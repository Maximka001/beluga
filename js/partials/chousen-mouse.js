define(["jquery"], function ($) {
    var $chousen = $('.js-chousen-mouse');
    var $left = $('.js-chouse-left');
    var $right = $('.js-chouse-right');

    if ($chousen.length && $left.length && $right.length) {
        $(document).on('mousemove', function (event) {

            if (window.innerWidth >= 427) {
                var x = event.clientX;
                var left = $left.offset().left + $left.width();
                var right = $right.offset().left;
                var total;
    
                if ((window.innerWidth - x) > right) {
                    total = left;
    
                } else if ((window.innerWidth - x) < left) {
                    total = right;
    
                } else {
                    total = x;
                }
    
                $chousen.css('width', total);
            } else {
                $chousen.removeAttr('style');
            }
        });

        $(window).on('resize', function () {

            if (window.innerWidth < 427) {
                $chousen.removeAttr('style');
            }
        });
    }
});