//- beluga: fullpage initialisation
//- 02-09-2016: Mel. Blimm
//- ---------------------
//- fullpage
define(['jquery', '../partials/cookies', '../partials/video', '../partials/is-mobile', 'fullpage', 'jquery-validation', 'jquery-touchswipe'], function ($, cookie, video, isMobile) {
    var isMob = function () {
        return window.innerWidth < 477 || isMobile();
    };
    var menuSelected = false;
    var productMobile = false;
    var playFilm = false;
    var pageAnchors = ['welcome', 'intro', 'video', 'about', 'idea', 'idea-2', 'product', 'gallery', 'feedback'];
    var isNoir = ($('.js-wrapper-noir').length)?true:false;
    var options = {
        anchors: pageAnchors,
        lockAnchors: (cookie.getCookie('is_adult') == undefined) ? true : false,
        css3: true,
        scrollingSpeed: 1000,
        fitToSection: false,
        fitToSectionDelay: 100,
        controlArrows: false,
        keyboardScrolling: (cookie.getCookie('is_adult') == undefined) ? false : true,
        onLeave: function (index, nextIndex, direction) {
            var currenSectionItem = $('.fullpage-wrapper section:eq(' + (nextIndex - 1) + ')');
            var currentAnchorLink = currenSectionItem.data('anchor');
            var currentSlideName = currenSectionItem.data('slide');
            var currentMenuItem = $('[data-menuanchor="' + currentSlideName + '"]');

            selectMenuItem(direction, currentMenuItem, currentSlideName, currentAnchorLink);
            menuSelected = true;

            if (!playFilm) video.pause($('#belugaVideo'));

            if (currentAnchorLink == 'gallery')
                $('.b-page-wrapper').addClass('fp-viewing-gallery');
            else
                $('.b-page-wrapper').removeClass('fp-viewing-gallery');
        },
        onSlideLeave: function (anchorLink, index, slideIndex, direction, nextSlideIndex) {
            $('.fp-slidesNav .active').removeClass('active');
            $('.fp-slidesNav li:eq(' + nextSlideIndex + ')').find('a').addClass('active');
        },
        afterLoad: function (anchorLink, index) {
            
            if (anchorLink == 'gallery') {
                $(document).swipe("enable");
            } else {
                $(document).swipe("disable");
            }

            if (playFilm) playFilm = false;
        }
    };

    function fullpageReBuild () {
        if ('fullpage' in $('.b-wrapper') && 'reBuild' in $('.b-wrapper').fullpage) {
            $('.b-wrapper').fullpage.reBuild();
        }
    }

    function resizeNoir() {
        if (isMob()) {
            if ('destroy' in $.fn.fullpage) {
                $.fn.fullpage.destroy('all');    
            }
        } else {
            if (!$('.b-wrapper.fullpage-wrapper').length || $('.b-wrapper.fp-destroyed').length) {
                $('.b-wrapper').fullpage(options);
            }
        }
    }
    
    function getFullpage() {

        if (isNoir) {
            resizeNoir();

        } else {

            if (!$('.b-wrapper.fullpage-wrapper').length || $('.b-wrapper.fp-destroyed').length) {
                $('.b-wrapper').fullpage(options);
            }
        }

        if (cookie.getCookie('is_adult') != '1') {
            $('.b-wrapper').fullpage.setAllowScrolling(false);
        }

        if ($(window).width() < 960) {
            productMobileSlides();
            fullpageReBuild();
        }
    }

    // выбор пункта меню
    function selectMenuItem(direction, currentMenuItem, currentSlideName, currentAnchorLink) {
        $('.js-navigation .active').removeClass('active');
        currentMenuItem.addClass('active');

        if (direction == 'down') {
            currentMenuItem.prevAll().each(function () {
                $(this).find('.b-nav__status').animate({ width: '100%' }, 250);
            });
        } else {
            currentMenuItem.nextAll().each(function () {
                $(this).find('.b-nav__status').animate({ width: 0 }, 250);
            });
        }

        var slideIndex = 1;
        $('[data-slide="' + currentSlideName + '"]').each(function (i) {
            if ($(this).data('anchor') == currentAnchorLink) {
                slideIndex = i + 1;
            }
        })
        var navWidth = 100 / $('[data-slide="' + currentSlideName + '"]').length * slideIndex;
        $('.js-navigation [data-menuanchor="' + currentSlideName + '"]').addClass('active').find('.b-nav__status').animate({ width: navWidth + '%' }, 250);

        // выбор фона
        $('.b-image-wrapper .active').removeClass('active');
        $('.b-image-wrapper [data-menuanchor="' + currentAnchorLink + '"]').addClass('active');
    }

    // удаление секции Welcome
    function deleteWelcomeSection() {
        $('[data-anchor="welcome"]').remove();
        $('.b-wrapper').fullpage.reBuild();
        $('.b-wrapper').fullpage.silentMoveTo('intro');
        $('.b-wrapper').fullpage.setAllowScrolling(true);
        $('.b-wrapper').fullpage.setKeyboardScrolling(true);
        $('.b-wrapper').fullpage.setLockAnchors(false);
    }

    function productMobileSlides() {
        productMobile = true;
        var product2 = $('[data-anchor="product"]').html();
        $('[data-anchor="product"]')
            .after('<section data-anchor="product2" data-slide="product" class="b-wrapper__item-page section fp-section fp-table">' +
                product2 + '</section>');

        $('[data-anchor="product"]').find('.b-container--mobile').hide();
        $('[data-anchor="product2"]').find('.b-container:first-child').hide();

        pageAnchors.splice(pageAnchors.indexOf('product') + 1, 0, "product2");
    }

    function productDesktopSlides() {
        productMobile = false;
        $('[data-anchor="product"]').find('.b-container').show();
        $('[data-anchor="product2"]').remove();
        pageAnchors.splice(pageAnchors.indexOf('product2'), 1);
    }

    if (cookie.getCookie('is_adult') != '1') {
        //убираем хэш
        history.pushState("", document.title, window.location.pathname);
    } else {
        //удаляем welcome
        pageAnchors.splice(pageAnchors.indexOf('welcome'), 1);
        $('[data-anchor="welcome"]').remove();
    }

    getFullpage();

    $('.b-play-film').on('click', function () {
        playFilm = true;
        video.play($('#belugaVideo'));
    });

    if (!menuSelected) {
        var direction = 'down';
        var anchorLink = (cookie.getCookie('is_adult') != '1') ? 'welcome' : 'intro';
        var slideName = $('[data-anchor="' + anchorLink + '"]').data('slide');
        var menuItem = $('[data-menuanchor="' + slideName + '"]');

        selectMenuItem(direction, menuItem, slideName, anchorLink);
    }

    // валидация формы Welcome
    var agree = false;
    $('[name="age-form"]').validate({
        errorElement: 'label',
        errorClass: 'error',
        messages: {
            age: {
                required: "*",
                min: false,
                number: false
            }
        },
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass).addClass('error-input');
        },
        submitHandler: function (form) {
            $('.b-wrapper__item-page--welcome .b-form__error').hide();

            // +1 день от текущего момента
            var date = new Date;
            date.setDate(date.getDate() + 1);
            if ($(form).find('[name="age"]').val() < 21) {
                return false;
            } else {
                if (!agree && $('[name="lang"]').val() == "RU") {
                    $('.b-popup--ru').fadeIn();
                    $('.b-button--ru.js-agree').on('click', function () {
                        form.submit();
                        agree = true;
                    });
                    return false;
                } else {
                    return true;
                }
            }
        },
        invalidHandler: function () {
            $('.b-wrapper__item-page--welcome .b-form__error').show();
        }
    });

    var resizeTimer;
    $(window).resize(function (e) {
        clearTimeout(resizeTimer);

        if (!isNoir) {
            
            if ($(window).width() < 960 && !productMobile) {
                productMobileSlides();
                fullpageReBuild();
            }
    
            if ($(window).width() >= 960 && productMobile) {
                productDesktopSlides();
                fullpageReBuild();
            }
        } else {
            resizeNoir();
        }


        resizeTimer = setTimeout(function () {
        }, 250);
    });
});