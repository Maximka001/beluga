define(["jquery", '../partials/is-mobile', 'slick-slider'], function ($, isMobile) {
    var $sliders = $('.js-slider-noir');
    var options = {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        centerMode: false,
        arrows: false,
        rows: 0,
        customPaging: function (slick, index) {
            return '<span></span>';
        }
    };

    function initSlider() {
        if ($sliders.length) {

            if (window.innerWidth < 477 || isMobile()) {
                
                if (!('slick' in $sliders[0])) {
                    $sliders.slick(options);
                } else {
                    $sliders.slick('refresh');
                }
            } else {

                if ('slick' in $sliders[0]) {
                    $sliders.slick('unslick');
                }
            }
        }
    }
    initSlider();

    $(window).on('resize', function () {
        initSlider();
    });
});