<?php
$main    = file_get_contents('main.html');
$welcome = file_get_contents('welcome.html');

// Age
if (isset($_REQUEST['age']) && !empty($_REQUEST['age'])) {
    setcookie('is_adult', 1, time() + 3600 * 24);
    $_COOKIE['is_adult'] = 1;
}

// Get language
$defaultLang = 'EN';

if (isset($_GET['lang']) && !empty($_GET['lang'])) {
    setcookie('lang', $_GET['lang'], time() + 3600 * 24);
    $lang = $_GET['lang'];
} elseif (isset($_POST['lang']) && !empty($_POST['lang'])) {
    setcookie('lang', $_POST['lang'], time() + 3600 * 24);
    $lang = $_POST['lang'];
} else {
    $lang = (isset($_COOKIE['lang']) && !empty($_COOKIE["lang"])) ? $_COOKIE['lang'] : $defaultLang;
}

include strtolower($lang).'.php';

if ($_COOKIE['is_adult'] != '1') {
    // $whereInsert = strripos($main, '<div class="b-wrapper">');
    // $content = substr_replace($main, $welcome, $whereInsert + 23, 0);
    $content = $welcome;
} else {
    $content = $main;
}

echo preg_replace(array_keys($translate), array_values($translate), $content);
?>