<?php $translate = array(
    '/{CSS}/' => '',
    '/{VIDEO_URL}/' => 'Lalique_FullHD_low.mov',
    '/{LANG}/'    => 'EN',
    '/{CUR_LANG}/'=> 'ru',
    '/{SHARE}/'   => 'Поделиться',
    '/{COPY}/'    => 'Все права защищены. Synergy Group 2016.',
    '/{SCROLL_TO_DISCOVER}/'   => '',
    '/{PLAY_FILM}/'   => '<span class="b-play-film__button"></span>',
    '/{PLAY}/'    => '',
    '/{PAUSE}/'   => '',
    '/{POPUP_MESSAGE}/'   => '<p>Cпасибо!</p><p>Ваше сообщение отправлено.</p>',
    '/{POPUP_BUTTON}/'    => 'Ok',
    '/{POPUP_SHARING}/'   => '<p>Выберите социальную сеть:</p>',
    '/{POPUP_PRE_ORDER}/'   => '<p>Спасибо!</p><p>Мы свяжемся с Вами в ближайшее время.</p>',
    '/{SOCIAL_FACEBOOK}/'   => 'Facebook',
    '/{SOCIAL_VKONTAKTE}/'  => 'Vkontakte',
    '/{INTRO}/' => 'Интро',
    '/{ABOUT_BELUGA}/' => 'О Beluga',
    '/{IDEA}/' => 'Идея',
    '/{PRODUCT}/' => 'Продукт',
    '/{CONTACT_US}/' => 'Контакты',
    '/{PRE_ORDER}/' => 'Предзаказ',
    '/{INTRO_TITLE}/' => 'Создание легенды',
    '/{INTRO_TEXT}/' => '<p>Выпущенная в&nbsp;количестве 1000 экземпляров лимитированная серия водки Beluga Epicure by&nbsp;Lalique&nbsp;&mdash; результат сотрудничества благородной русской водки Beluga и&nbsp;легендарного французского производителя хрусталя и&nbsp;ювелирных украшений Lalique.</p>
        <p>Культовые бренды представляют уникальный совместный проект как воплощение ценностей, которые они разделяют: бережное отношение к&nbsp;наследию и&nbsp;истории, стремление к&nbsp;бескомпромиссному качеству и&nbsp;внимание к&nbsp;каждой детали, постоянный поиск вдохновения.</p>',
    '/{ABOUT_TEXT}/' => '<p>Beluga&nbsp;&mdash; водка номер один в&nbsp;сегментах супер- и&nbsp;ультрапремиум в&nbsp;России. Непревзойденные вкус и&nbsp;качество Beluga превратили ее&nbsp;в&nbsp;синоним роскоши и&nbsp;успеха во&nbsp;всем мире. Производство Beluga расположено на&nbsp;легендарном Мариинском ликеро-водочном заводе, ведущем свою историю с&nbsp;1900&nbsp;года. Расположенное в&nbsp;самом центре Сибири, на&nbsp;расстоянии 300&nbsp;км от&nbsp;ближайшего города, производство находится в&nbsp;одном из&nbsp;самых уединенных и&nbsp;экологически чистых мест в&nbsp;России.</p>
        <p>Передовое оборудование в&nbsp;сочетании с&nbsp;бережно хранимыми традициями производства позволяют создать поистине эксклюзивный продукт, воплощение мастерства и&nbsp;благородства. Натуральные ингредиенты, чистейшая артезианская вода и&nbsp;период &laquo;отдыха&raquo;, составляющий от&nbsp;30&nbsp;до&nbsp;90&nbsp;дней в&nbsp;зависимости от&nbsp;рецептуры, позволяют добиться неповторимого бархатистого вкуса.</p>',
    '/{IDEA_TEXT}/' => '<p>Концепция декантера отражает наследие дома Lalique и&nbsp;уникальные методы и&nbsp;рецептуру производства Beluga. В&nbsp;дизайне Beluga Epicure by&nbsp;Lalique нашли свое новое воплощение знаковые для Lalique темы&nbsp;&mdash; женщина, природа и&nbsp;цветы. Изысканный сосуд украшает изображение загадочной обнаженной женской натуры, окруженной пшеничными колосьями, символом урожая и&nbsp;плодородия.</p>',
    '/{IDEA_2_TEXT}/' => '<p>Выбранное для лимитированной серии водки название&nbsp;&mdash; Epicure&nbsp;&mdash; подчеркивает ценность и&nbsp;красоту ускользающего мгновения. Каждый декантер из&nbsp;серии&nbsp;&mdash; напоминание о&nbsp;том, как важно уметь наслаждаться настоящим моментом.</p>',
    '/{PRODUCT_TEXT}/' => '<p>Каждый декантер производится вручную в&nbsp;мастерских Lalique, расположенных на&nbsp;северо-востоке Франции, в&nbsp;Эльзасе. Раскаленный хрусталь вручную заливается в&nbsp;специальную металлическую форму для отлива&nbsp;&mdash; так создается основа будущего декантера.
    <span>Затем мастера-полировщики добиваются утонченного контраста между сатинированным и&nbsp;прозрачным стеклом, чтобы подчеркнуть одновременно монументальность и&nbsp;хрупкость каждого из&nbsp;сосудов.</span>
    Финальный штрих&nbsp;&mdash; гравировка индивидуального порядкового номера на&nbsp;дне каждого декантера.</p>
    <p>Уникальная рецептура Beluga Epicure была воссоздана по&nbsp;материалам, хранящимся в&nbsp;архивах Мариинского завода. Чистейшая артезианская вода из&nbsp;сибирских источников, трудоемкий процесс производства и&nbsp;период отдыха, длящийся 100&nbsp;дней, позволяют создавать водку уникального качества&nbsp;&mdash; воплощение опыта и&nbsp;мастерства Beluga.</p>
    <p>Лимитированная серия водки Beluga Epicure by&nbsp;Lalique&nbsp;&mdash; настоящее произведение искусства, посвященное двумя брендами своим поклонникам, истинным ценителям и&nbsp;коллекционерам.</p>',
    '/{FORM_ERROR}/'    => '',
    '/{FORM_FEEDBACK_BUTTON}/'    => 'Отправить сообщение ',
    '/{FORM_PRE_ORDER_BUTTON}/'    => 'Предзаказ',
    '/{FORM_COMMENT}/'    => 'Ваше сообщение',
    '/{FORM_NAME}/'    => 'Ваше имя',
    '/{FORM_EMAIL}/'    => 'Адрес электронной почты',
    '/{FORM_COUNTRY}/'    => 'Страна, город',
    '/{FORM_PHONE}/'    => 'Номер телефона',
    '/{CONTACT_US_TITLE}/' => 'Написать письмо Beluga',
    '/{FORM_SPAM}/' => 'Это не спам сообщение.',
    '/{POPUP_BUTTON_RU_2}/' => 'Подтверждаю',
    '/{POPUP_BUTTON_RU_1}/' => 'Не подтверждаю',
    '/{CHOUSE_TITLE_LEFT}/' => 'BELUGA EPICURE<br>BY LALIQUE',
    '/{CHOUSE_TITLE_RIGHT}/' => 'BELUGA EPICURE<br>NOIR<br>BY LALIQUE',
    '/{CHOUSE_LINK}/' => 'Читать далее',
    '/{PREVIE_TEXT_1}/' => 'Лимитированная серия Beluga Epicure Noir – результат сотрудничества двух легендарных брендов - русской водки Beluga и французского ювелирного дома Lalique',
    '/{PREVIE_TEXT_1_1}/' => '<span class="upper-mob">Лимитированная серия Beluga Epicure Noir by lalique</span><br>– результат сотрудничества двух легендарных брендов - русской водки Beluga и французского ювелирного дома Lalique',
    '/{PREVIE_TEXT_2}/' => 'Каждый декантер из серии - напоминание о том, как важно уметь наслаждаться настоящим моментом. Beluga Epicure Noir – это воплощение ценностей, которые разделяют эти два бренда: стремление к совершенству, вековые традиции мастерства и внимание к каждой детали',
    '/{PREVIE_CONT_TITLE_1}/' => 'Процесс создания',
    '/{PREVIE_CONT_TEXT_1}/' => '<p>чёрных декантеров отличается необычайной трудоёмкостью и сложностью, поскольку каждый декантер производится вручную в мастерских Lalique, расположенных на северо-востоке Франции, в Эльзасе.  Неповторимой особенностью данного декантера является его черный цвет, который достигается за счет использования кобальта в составе основы, используемой для создания сосуда. Работа с жидким чёрным хрусталем требует больших усилий и времени, чем производство декантеров из прозрачного хрусталя.</p><p>После нанесения платины на декор крышки и рисунок декантера - сосуд отправляют на обжиг, где высока вероятность повреждения изделия, именно поэтому от мастеров требуется особая внимательность и точность. С учетом всех стадий производства 90% декантеров получаются с изъянами и лишь 10 экземпляров из 100 становятся идеальными сосудами для лимитированного купажа водки Beluga. Всё это позволяет оценить уникальность новой серии Beluga Epicure Noir, где каждый декантер содержит свой индивидуальный номер.</p>',
    '/{PREVIE_CONT_TEXT_2}/' => 'чёрных декантеров отличается необычайной трудоёмкостью и сложностью, поскольку каждый декантер производится вручную в мастерских Lalique, расположенных на северо-востоке Франции, в Эльзасе.  Неповторимой особенностью данного декантера является его черный цвет, который достигается за счет использования кобальта в составе основы, используемой для создания сосуда.', 
    '/{PREVIE_CONT_TEXT_3}/' => 'Работа с жидким чёрным хрусталем требует больших усилий и времени, чем производство декантеров из прозрачного хрусталя.',
    '/{PREVIE_CONT_TEXT_4}/' => 'После нанесения платины на декор крышки и рисунок декантера - сосуд отправляют на обжиг, где высока вероятность повреждения изделия, именно поэтому от мастеров требуется особая внимательность и точность.', 
    '/{PREVIE_CONT_TEXT_5}/' => 'С учетом всех стадий производства 90% декантеров получаются с изъянами и лишь 10 экземпляров из 100 становятся идеальными сосудами для лимитированного купажа водки Beluga. Всё это позволяет оценить уникальность новой серии Beluga Epicure Noir, где каждый декантер содержит свой индивидуальный номер.',
    '/{PREVIE_CONT_TITLE_2}/' => 'Водка Beluga',
    '/{PREVIE_CONT_TEXT_6}/' => '<p>находящаяся в составе Beluga Epicure Noir, создана по редкому рецепту, который технологи Мариинского ликеро-водочного завода передавали друг другу из поколения в поколения. Уникальный солодовый спирт, чистейшая артезианская вода из глубинных источников Сибири, а также фильтрация черным кварцевым песком делают жидкость Beluga Epicure Noir абсолютно эксклюзивным продуктом на российском и зарубежном рынке. Период отдыха на 100 дней превышает Beluga Epicure by Lalique и составляет 200 дней.</p><p>Лимитированная серия водки Beluga Epicure Noir выпущена в количестве всего 10 экземпляров, созданных для настоящих поклонников коллаборации двух брендов, истинных ценителей  и коллекционеров. Каждый декантер Beluga Epicure Noir - воплощение безграничного стремления к совершенству, многолетнего опыта, истинного мастерства и качества, которые создаются, а не производятся.</p>',
    '/{PREVIE_CONT_TEXT_7}/' => 'находящаяся в составе Beluga Epicure Noir, создана по редкому рецепту, который технологи Мариинского ликеро-водочного завода передавали друг другу из поколения в поколения. Уникальный солодовый спирт, чистейшая артезианская вода из глубинных источников Сибири, а также фильтрация черным кварцевым песком делают жидкость Beluga Epicure Noir абсолютно эксклюзивным продуктом на российском и зарубежном рынке.',
    '/{PREVIE_CONT_TEXT_8}/' => 'Период отдыха на 100 дней превышает Beluga Epicure by Lalique и составляет 200 дней. Лимитированная серия водки Beluga Epicure Noir выпущена в количестве всего 10 экземпляров, созданных для настоящих поклонников коллаборации двух брендов, истинных ценителей  и коллекционеров. Каждый декантер Beluga Epicure Noir - воплощение безграничного стремления к совершенству, многолетнего опыта, истинного мастерства и качества, которые создаются, а не производятся.'
);
return $translate;
?>